import json

from Help_Scrabble.Modules.User_input import UserInput
from Help_Scrabble.Sms.messages import Messages
from Help_Scrabble.Modules.Search_words import SearchWords


Messages.presentation() 
## player input the pawn's letter
user_word = UserInput.enter_word() 

## this program search the all words whose constituate by player's letter
search_words = SearchWords(user_word, is_english_word=False)

## the result contains in `word_found`
word_found = search_words.search() 

## beautiful display result
print(json.dumps(word_found, indent=3))
