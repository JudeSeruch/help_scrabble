class UserInput:
    @staticmethod
    def enter_word():
        user_word = str(input("Enter the letters you have : "))
        if len(user_word) > 7:
            user_word = UserInput.enter_word()
        return user_word
