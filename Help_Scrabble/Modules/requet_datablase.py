from os.path import join


class RequestDataBase:
    french_data_path = "D:/Python/Python Data/all_words/ods6.txt"
    english_data_path = "D:/Python/Python Data/all_words/ospd3_expurgated.txt"
    parent_french_path = "data/french_data/"
    parent_english_path = "data/english_data/"

    def __init__(self, is_english_word: bool):
        self.is_english_word = is_english_word

    def refactor_data(self):
        data_path = self.english_data_path if self.is_english_word else self.french_data_path
        with open(data_path, 'r') as data_file:
            words = data_file.read().splitlines()
            data_file.close()

        data_asset = self.parent_english_path if self.is_english_word else self.parent_french_path
        for word in words:
            word = word.lower()
            i = word[0]
            with open(join(data_asset, i+'.txt'), 'a') as data_file:
                data_file.write(word+'\n')
                data_file.close()

    def letter_words(self, letter: str) -> list:
        data_path = self.parent_english_path if self.is_english_word else self.parent_french_path
        with open(join(data_path, letter+'.txt'), 'r') as letter_word:
            return letter_word.read().splitlines()
