from Help_Scrabble.Modules.requet_datablase import RequestDataBase
from copy import deepcopy


class SearchWords:
    point_pawn_french = {'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 1, 'g': 2, 'h': 4, 'i': 1,
                         'j': 8, 'k': 10, 'l': 1, 'm': 2, 'n': 1, 'o': 1, 'p': 3, 'q': 8, 'r': 1, 's': 1,
                         't': 1, 'u': 1, 'v': 4, 'w': 10, 'x': 10, 'y': 10, 'z': 10
                         }
    point_pawn_english = {'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1,
                          'j': 8, 'k': 8, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1,
                          't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
                          }

    def __init__(self, user_word: str, is_english_word: bool):
        self.is_english_word = is_english_word
        self.request_database = RequestDataBase(is_english_word=self.is_english_word)
        self.user_word = user_word
        self.max_length_word = 7
        self.number_jockey = self.user_word.count(' ')

    def search(self) -> dict:

        """
            :return: the list of all word who form by user_word
        """

        letter_treated = []
        word_found = []
        words = []
        
        for letter in self.user_word:
            if letter not in letter_treated:
                letter_treated.append(letter)
                if letter != ' ':
                    # loaded all words started by `letter`
                    words = self.request_database.letter_words(letter=letter)

                
                for word in words:
                    is_in = []
                    word_copy = deepcopy(self.user_word)
                    for j in word:
                        if j in word_copy and len(word) <= self.max_length_word:
                            word_copy = word_copy.replace(j, '', 1)
                            is_in.append(True)
                        else:
                            is_in.append(False)
                    if is_in.count(False) <= self.number_jockey:
                        word_found.append(word)
        # print(word_found)
        return self.arrange(word_found)

    def arrange(self, word_founds: list):
        """
        :param word_founds: all words found by search function
        :return: ascending points sorted the words
        """
        word_points = {}
        point_pawn = self.point_pawn_english if self.is_english_word else self.point_pawn_french
        for word in word_founds:
            point = 50 if len(word) == self.max_length_word else 0
            ordLetter, letterIsJockey = self.isJockey(word)
            for index in range(len(word)):
                point = point + point_pawn[word[index]] if not letterIsJockey[index] else point
                # point += point_pawn[letter]
            word_points[word] = point
        return dict(sorted(word_points.items(), key=lambda x: x[1], reverse=True))

    def isJockey(self, word_found: str):
        userWordLetterCount = {}
        wordFoundLetterCount = {}

        wordLetter = []
        letterIsJockey = []

        for letter in self.user_word:
            if letter != ' ':
                userWordLetterCount[letter] = userWordLetterCount[letter] + 1 if letter in userWordLetterCount.keys() else 1
        
        for letter in word_found:
            wordFoundLetterCount[letter] = wordFoundLetterCount[letter] + 1 if letter in wordFoundLetterCount.keys() else 1
            wordLetter.append(letter)
            if letter not in self.user_word:
                letterIsJockey.append(False)
            else:
                letterIsJockey.append(wordFoundLetterCount[letter] > userWordLetterCount[letter])

        return wordLetter, letterIsJockey